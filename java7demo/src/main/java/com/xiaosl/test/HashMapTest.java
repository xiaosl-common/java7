package com.xiaosl.test;

import java.util.HashMap;
import java.util.Map;

/**
 * User: xiaosl
 * Date: 2019-10-24-8:01
 * Description: 
 */
public class HashMapTest {

	public static void main(String[] args) {
		Map<Integer, Integer> initMap = new HashMap<Integer, Integer>();
		/*for (int i = 0; i <= 23; i++) {
			initMap.put(i + "", i + "");
		}
		Map<String, String> copyMap = new HashMap<String, String>(initMap);
		copyMap.put(null, null);*/
		initMap.put(null, 123);
		for (int i = 0; i <= 16; i++) {
			if (i == 15) {
				System.out.println(i);
			}
			initMap.put(i, i);
		}
		Map<Integer, Integer> copyMap = new HashMap<Integer, Integer>(initMap);
		initMap.put(null, 234);
		System.out.println(initMap.get(null));
		System.out.println(copyMap.get(null));


	}
}
