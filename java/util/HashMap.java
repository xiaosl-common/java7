/*
 * Copyright (c) 1997, 2010, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.util;

import java.util.HashMap.HashIterator;

/**
 * Hash table based implementation of the <tt>Map</tt> interface.  This
 * implementation provides all of the optional map operations, and permits
 * <tt>null</tt> values and the <tt>null</tt> key.  (The <tt>HashMap</tt>
 * class is roughly equivalent to <tt>Hashtable</tt>, except that it is
 * unsynchronized and permits nulls.)  This class makes no guarantees as to
 * the order of the map; in particular, it does not guarantee that the order
 * will remain constant over time.
 *
 * <p>This implementation provides constant-time performance for the basic
 * operations (<tt>get</tt> and <tt>put</tt>), assuming the hash function
 * disperses the elements properly among the buckets.  Iteration over
 * collection views requires time proportional to the "capacity" of the
 * <tt>HashMap</tt> instance (the number of buckets) plus its size (the number
 * of key-value mappings).  Thus, it's very important not to set the initial
 * capacity too high (or the load factor too low) if iteration performance is
 * important.
 *
 * <p>An instance of <tt>HashMap</tt> has two parameters that affect its
 * performance: <i>initial capacity</i> and <i>load factor</i>.  The
 * <i>capacity</i> is the number of buckets in the hash table, and the initial
 * capacity is simply the capacity at the time the hash table is created.  The
 * <i>load factor</i> is a measure of how full the hash table is allowed to
 * get before its capacity is automatically increased.  When the number of
 * entries in the hash table exceeds the product of the load factor and the
 * current capacity, the hash table is <i>rehashed</i> (that is, internal data
 * structures are rebuilt) so that the hash table has approximately twice the
 * number of buckets.
 *
 * <p>As a general rule, the default load factor (.75) offers a good tradeoff
 * between time and space costs.  Higher values decrease the space overhead
 * but increase the lookup cost (reflected in most of the operations of the
 * <tt>HashMap</tt> class, including <tt>get</tt> and <tt>put</tt>).  The
 * expected number of entries in the map and its load factor should be taken
 * into account when setting its initial capacity, so as to minimize the
 * number of rehash operations.  If the initial capacity is greater
 * than the maximum number of entries divided by the load factor, no
 * rehash operations will ever occur.
 *
 * <p>If many mappings are to be stored in a <tt>HashMap</tt> instance,
 * creating it with a sufficiently large capacity will allow the mappings to
 * be stored more efficiently than letting it perform automatic rehashing as
 * needed to grow the table.
 *
 * <p><strong>Note that this implementation is not synchronized.</strong>
 * If multiple threads access a hash map concurrently, and at least one of
 * the threads modifies the map structurally, it <i>must</i> be
 * synchronized externally.  (A structural modification is any operation
 * that adds or deletes one or more mappings; merely changing the value
 * associated with a key that an instance already contains is not a
 * structural modification.)  This is typically accomplished by
 * synchronizing on some object that naturally encapsulates the map.
 *
 * If no such object exists, the map should be "wrapped" using the
 * {@link Collections#synchronizedMap Collections.synchronizedMap}
 * method.  This is best done at creation time, to prevent accidental
 * unsynchronized access to the map:<pre>
 *   Map m = Collections.synchronizedMap(new HashMap(...));</pre>
 *
 * <p>The iterators returned by all of this class's "collection view methods"
 * are <i>fail-fast</i>: if the map is structurally modified at any time after
 * the iterator is created, in any way except through the iterator's own
 * <tt>remove</tt> method, the iterator will throw a
 * {@link ConcurrentModificationException}.  Thus, in the face of concurrent
 * modification, the iterator fails quickly and cleanly, rather than risking
 * arbitrary, non-deterministic behavior at an undetermined time in the
 * future.
 *
 * <p>Note that the fail-fast behavior of an iterator cannot be guaranteed
 * as it is, generally speaking, impossible to make any hard guarantees in the
 * presence of unsynchronized concurrent modification.  Fail-fast iterators
 * throw <tt>ConcurrentModificationException</tt> on a best-effort basis.
 * Therefore, it would be wrong to write a program that depended on this
 * exception for its correctness: <i>the fail-fast behavior of iterators
 * should be used only to detect bugs.</i>
 *
 * <p>This class is a member of the
 * <a href="{@docRoot}/../technotes/guides/collections/index.html">
 * Java Collections Framework</a>.
 *
 * @param <K> the type of keys maintained by this map
 * @param <V> the type of mapped values
 *
 * @author Doug Lea
 * @author Josh Bloch
 * @author Arthur van Hoff
 * @author Neal Gafter
 * @see     Object#hashCode()
 * @see     Collection
 * @see     Map
 * @see     TreeMap
 * @see     Hashtable
 * @since 1.2
 */

public class HashMap<K, V>
		extends AbstractMap<K, V>
		implements Map<K, V>, Cloneable, Serializable {

	/**
	 *默认初始容量-必须是2的幂。默认值为16（为什们是16？）
	 */
	static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16

	/**
	 *如果隐式指定较高值，则使用最大容量。*(超过int的最大值)
	 *由任何一个带参数的构造函数。
	 *必须是2的幂<=1<<30。
	 * 2^30
	 * 为什么是30：https://blog.csdn.net/saywhat_sayhello/article/details/83120324
	 * capacity就是指HashMap中桶的数量。和size关系是  capacity>=size
	 */
	static final int MAXIMUM_CAPACITY = 1 << 30;

	/**
	 * 构造函数中未指定时使用的加载因子。
	 */
	static final float DEFAULT_LOAD_FACTOR = 0.75f;

	/**
	 * 当表未扩容时共享的空表实例。
	 */
	static final Entry<?, ?>[] EMPTY_TABLE = {};

	/**
	 * 表，根据需要调整大小。长度必须始终是2的幂。
	 */
	transient Entry<K, V>[] table = (Entry<K, V>[]) EMPTY_TABLE;

	/**
	 * The number of key-value mappings contained in this map.
	 * size表示HashMap中存放KV的数量
	 *
	 * TODO 网上关于hashMap都是说不安全是说 put方法的时候的扩容时候引发的不安全的问题？
	 * 那么在并发插入的时候 hashMap的size是线程安全的么？
	 */
	transient int size;


	/**
	 * threshold表示当HashMap的size大于threshold时会执行resize操作。
	 * threshold=capacity*loadFactor
	 * 如果table == EMPTY_TABLE，那么就会用这个值作为初始容量，创建新的哈希表
	 *
	 */
	int threshold;

	/**
	 * 哈希表的加载因子。
	 * @serial
	 */
	final float loadFactor;

	/**
	 *此哈希映射在结构上被修改的次数
	 *结构修改是改变
	 *哈希映射或以其他方式修改其内部结构（例如，
	 *重新整理）。此字段用于对的集合视图生成迭代器
	 *哈希映射失败得很快。（参见ConcurrentModificationException）。
	 */
	transient int modCount;

	/**
	 * The default threshold of map capacity above which alternative hashing is
	 * used for String keys. Alternative hashing reduces the incidence of
	 * collisions due to weak hash code calculation for String keys.
	 * <p/>
	 * This value may be overridden by defining the system property
	 * {@code jdk.map.althashing.threshold}. A property value of {@code 1}
	 * forces alternative hashing to be used at all times whereas
	 * {@code -1} value ensures that alternative hashing is never used.
	 * 大意是说，ALTERNATIVE_HASHING_THRESHOLD_DEFAULT是一个默认的阈值，当一个键值对的键是String类型时，且map的容量达到了这个阈值，
	 * 就启用备用哈希（alternative hashing）。备用哈希可以减少String类型的key计算哈希码（更容易）发生哈希碰撞的发生率。
	 * 该值可以通过定义系统属性jdk.map.althashing.threshold来指定。如果该值是1，表示强制总是使用备用哈希；如果是-1则表示禁用。
	 */
	static final int ALTERNATIVE_HASHING_THRESHOLD_DEFAULT = Integer.MAX_VALUE;

	/**
	 * 保存在启动vm之前无法初始化的值。（Holder维护着一些只有在虚拟机启动后才能初始化的值）
	 */
	private static class Holder {

		/**
		 * Table capacity above which to switch to use alternative hashing.
		 * 触发启用备用哈希的哈希表容量阈值
		 */
		static final int ALTERNATIVE_HASHING_THRESHOLD;

		static {
			// 读取JVM参数 -Djdk.map.althashing.threshold
			String altThreshold = java.security.AccessController.doPrivileged(
					new sun.security.action.GetPropertyAction(
							"jdk.map.althashing.threshold"));

			int threshold;
			try {
				// 如果该参数没有值，采用默认值
				threshold = (null != altThreshold) ? Integer.parseInt(altThreshold) : ALTERNATIVE_HASHING_THRESHOLD_DEFAULT;
				// disable alternative hashing if -1
				//如果参数值为 - 1，禁用备用哈希
				// ALTERNATIVE_HASHING_THRESHOLD_DEFAULT也是等于Integer.MAX_VALUE
				// 所以jdk默认是禁用备用哈希的
				if (threshold == -1) {
					threshold = Integer.MAX_VALUE;
				}

				// 参数为其它负数，则视为非法参数
				if (threshold < 0) {
					throw new IllegalArgumentException("value must be positive integer.");
				}
			} catch (IllegalArgumentException failed) {
				throw new Error("Illegal value for 'jdk.map.althashing.threshold'", failed);
			}

			ALTERNATIVE_HASHING_THRESHOLD = threshold;
		}
	}


	/**
	 *备用hash值。该值目的是为了进一步减少哈希碰撞。如果hashSeed=0表示禁用备用哈希。
	 * https://blog.csdn.net/qq_30447037/article/details/78985216
	 */
	transient int hashSeed = 0;

	/**
	 * 构造函数
	 * 用指定的初始值（容量和加载因子。）构造一个空的hashmap
	 */
	public HashMap(int initialCapacity, float loadFactor) {
		if (initialCapacity < 0) {
			throw new IllegalArgumentException("Illegal initial capacity: " + initialCapacity);
		}
		if (initialCapacity > MAXIMUM_CAPACITY) {
			initialCapacity = MAXIMUM_CAPACITY;
		}
		if (loadFactor <= 0 || Float.isNaN(loadFactor)) {
			throw new IllegalArgumentException("Illegal load factor: " + loadFactor);
		}
		this.loadFactor = loadFactor;
		threshold = initialCapacity;
		init();
	}

	/**
	 *用指定的初始值构造一个空的hashmap 负载因子为默认值0.75
	 */
	public HashMap(int initialCapacity) {
		this(initialCapacity, DEFAULT_LOAD_FACTOR);
	}

	/**
	 * 构造一个空的hashmap 初始容量为默认值16  负载因子为默认值0.75
	 */
	public HashMap() {
		this(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR);
	}

	/**
	 *用一个相同的 K,V值的map去创建一个hashMap，
	 * 创建的默认hashMap初始容量和map保持一致，
	 * 负载因子的值为默认的0.75
	 * @param   m the map whose mappings are to be placed in this map
	 * @throws NullPointerException if the specified map is null
	 */
	public HashMap(Map<? extends K, ? extends V> m) {
		//TODO  为什么是size/0.75f + 1 然后和默认的初始容量16区最大值
		int initialCapacity = Math.max((int) (m.size() / DEFAULT_LOAD_FACTOR) + 1, DEFAULT_INITIAL_CAPACITY);
		//初始化指定值大小的hashMap
		this(initialCapacity, DEFAULT_LOAD_FACTOR);
		//初始化哈希表，分配哈希表内存空间
		inflateTable(threshold);
		//设置值
		putAllForCreate(m);
	}

	/**
	 * 将一个数换算成2的n次幂,若number 大于默认的初始容量  则直接获取初始容量，否则获取number  最为靠近的 2的次幂的 int值
	 * @param number
	 * @return
	 */
	private static int roundUpToPowerOf2(int number) {
		// assert number >= 0 : "number must be non-negative";
		// 理解 Integer.highestOneBit((number - 1) << 1)
		// 比如 number = 23，23 - 1 = 22，二进制是：10110
		// 22 左移一位（右边补1个0），结果是：101100
		// Integer.highestOneBit() 函数的作用是取左边最高一位，其余位取0，
		// 即：101100 -> 100000，换成十进制就是 32
		return number >= MAXIMUM_CAPACITY ? MAXIMUM_CAPACITY : (number > 1) ? Integer.highestOneBit((number - 1) << 1) : 1;
	}

	/**
	 * Inflates the table.
	 *初始化哈希表，分配哈希表内存空间
	 * https://segmentfault.com/a/1190000018520768
	 */
	private void inflateTable(int toSize) {
		// 找出大于等于toSize的2的n次幂，作为哈希表的容量
		int capacity = roundUpToPowerOf2(toSize);
		// 计算新的扩容阈值： 容量 * 负载因子
		threshold = (int) Math.min(capacity * loadFactor, MAXIMUM_CAPACITY + 1);
		// 指定容量建哈希表 其实就是创建一个指定的空的数组
		//该地方是新创建的一个table 也就是说是新开辟了一个内存空间，验证了clone方法上说的这个地方是非浅拷贝
		table = new Entry[capacity];
		// 根据容量判断是否需要初始化hashSeed
		initHashSeedAsNeeded(capacity);
	}

	void init() {
	}

	/**
	 * Initialize the hashing mask value. We defer initialization until we
	 * really need it.
	 *  按需初始化hashSeed
	 */
	final boolean initHashSeedAsNeeded(int capacity) {
		//当我们初始化的时候hashSeed为0,0!=0 这时为false,表示当前正在使用备用哈希
		boolean currentAltHashing = hashSeed != 0;
		//如果vm启动了且map的容量大于阈值，使用备用哈希
		boolean useAltHashing = sun.misc.VM.isBooted() && (capacity >= Holder.ALTERNATIVE_HASHING_THRESHOLD);
		// 异或操作，如果两值同时为false，或同时为true，都算是false。
		boolean switching = currentAltHashing ^ useAltHashing;
		if (switching) {
			// 把hashSeed设置成随机值
			hashSeed = useAltHashing ? sun.misc.Hashing.randomHashSeed(this) : 0;
		}
		return switching;
	}

	/**
	 * Retrieve object hash code and applies a supplemental hash function to the
	 * result hash, which defends against poor quality hash functions.  This is
	 * critical because HashMap uses power-of-two length hash tables, that
	 * otherwise encounter collisions for hashCodes that do not differ
	 * in lower bits. Note: Null keys always map to hash 0, thus index 0.
	 * 获取key的哈希码，并应用一个补充的哈希函数，构成最终的哈希码。
	 */
	final int hash(Object k) {

		// 如果哈希种子是随机值，使用备用哈希
		// （方法调用链：inflateTable()-->initHashSeedAsNeeded()-->hash()，
		// 在initHashSeedAsNeeded()中已判断了是否需要初始化哈希种子）
		int h = hashSeed;
		if (0 != h && k instanceof String) {
			return sun.misc.Hashing.stringHash32((String) k);
		}

		h ^= k.hashCode();
		h ^= (h >>> 20) ^ (h >>> 12);
		return h ^ (h >>> 7) ^ (h >>> 4);
	}

	/**
	 * Returns index for hash code h.
	 * 根据key的hash值和数组长度获取当前key在数组下标的位置
	 * TODO 为什么 & 这种计算能获取到下标值
	 * @param h key的hash值
	 * @param length 数组的长度
	 * @return
	 */
	static int indexFor(int h, int length) {
		return h & (length - 1);
	}

	/**
	 * Returns the number of key-value mappings in this map.
	 *获取map的大小
	 * @return the number of key-value mappings in this map
	 */
	public int size() {
		return size;
	}

	/**
	 * Returns <tt>true</tt> if this map contains no key-value mappings.
	 *判断map是不是空的 ，通过他的size
	 * @return <tt>true</tt> if this map contains no key-value mappings
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Returns the value to which the specified key is mapped,
	 * or {@code null} if this map contains no mapping for the key.
	 *
	 * <p>More formally, if this map contains a mapping from a key
	 * {@code k} to a value {@code v} such that {@code (key==null ? k==null :
	 * key.equals(k))}, then this method returns {@code v}; otherwise
	 * it returns {@code null}.  (There can be at most one such mapping.)
	 *
	 * <p>A return value of {@code null} does not <i>necessarily</i>
	 * indicate that the map contains no mapping for the key; it's also
	 * possible that the map explicitly maps the key to {@code null}.
	 * The {@link #containsKey containsKey} operation may be used to
	 * distinguish these two cases.
	 *
	 * @see #put(Object, Object)
	 * 重点！！！
	 * 上面大概的意思是：通过key值映射返回一个value值或者是null；
	 * null值有2种情况：
	 *  1、返回的映射值就是null;
	 *  2、hash表没有此映射；
	 * 可以通过containsKey的方区分这2种情况
	 *
	 * 说明了 hashMap的key和value 都是可以为null1的
	 */
	public V get(Object key) {
		//说明： hashMap的key为null1的时候 都是存储在table【0】的位置的。为什么请查看putForNullKey方法
		if (key == null)
			//若key为null 则执行 key为null的特殊处理的方法
			return getForNullKey();
		//若key值不为null则 执行正常获取 key对应的entry对象方法
		Entry<K, V> entry = getEntry(key);
		//获取entry对应的value
		return null == entry ? null : entry.getValue();
	}

	/**
	 * Offloaded version of get() to look up null keys.  Null keys map
	 * to index 0.  This null case is split out into separate methods
	 * for the sake of performance in the two most commonly used
	 * operations (get and put), but incorporated with conditionals in
	 * others.
	 * //针对于key值为null的取值方法
	 */
	private V getForNullKey() {
		//若hashMap的大小是0  则说明无可取之值  直接返回
		if (size == 0) {
			return null;
		}
		//获取 table[0]位置的值（key为null的值是存储在table[0]的位置的链表里），遍历链表获取值
		for (Entry<K, V> e = table[0]; e != null; e = e.next) {
			if (e.key == null)
				return e.value;
		}
		return null;
	}

	/**
	 * Returns <tt>true</tt> if this map contains a mapping for the
	 * specified key.
	 *
	 * @param   key   The key whose presence in this map is to be tested
	 * @return <tt>true</tt> if this map contains a mapping for the specified
	 * key.
	 * 判断 hashMap是否有key值
	 * 疑问：在上面get的注释有说明 ，当执行get方法的时候返回值为null。containsKey可以区分一下两种情况：
	 *  1、有key为null；比如map.put(null,1) 2、map里面没有此key，因此返回null？
	 *
	 *  解答：
	 *  执行getEntry方法的时候，对于key为null的情况，hash=0 对应的下标index也是0，再遍历table[0]的链表返回的是一个entry，故containsKey返回的是一个true；
	 *  而对于2的则entry是返回null  故containsKey返回的是一个false； 所以说 containsKey可以区分get(key)方法返回null到底是个什么情况
	 */
	public boolean containsKey(Object key) {
		return getEntry(key) != null;
	}

	/**
	 * Returns the entry associated with the specified key in the
	 * HashMap.  Returns null if the HashMap contains no mapping
	 * for the key.
	 *
	 * 根据key获取一个entry对象
	 */
	final Entry<K, V> getEntry(Object key) {
		//hashMap大小为0 直接返回
		if (size == 0) {
			return null;
		}
		//获取key的hash值（这个地方什么时候为key为null？ containsKey方法执行的时候key可能为null）
		int hash = (key == null) ? 0 : hash(key);
		//遍历hashMap(准确的说是遍历key对于的下标的值table[index]的链表，所以说为什么hashMap的get复杂度是O(n)) 获取entry
		for (Entry<K, V> e = table[indexFor(hash, table.length)]; e != null; e = e.next) {
			Object k;
			if (e.hash == hash && ((k = e.key) == key || (key != null && key.equals(k))))
				return e;
		}
		return null;
	}

	/**
	 * Associates the specified value with the specified key in this map.
	 * If the map previously contained a mapping for the key, the old
	 * value is replaced.
	 *
	 * @param key key with which the specified value is to be associated
	 * @param value value to be associated with the specified key
	 * @return the previous value associated with <tt>key</tt>, or
	 *         <tt>null</tt> if there was no mapping for <tt>key</tt>.
	 *         (A <tt>null</tt> return can also indicate that the map
	 *         previously associated <tt>null</tt> with <tt>key</tt>.)
	 */
	public V put(K key, V value) {
		if (table == EMPTY_TABLE) {
			//如果table是个空的，则执行初始化hashMap空间（这个操作在初始化一个hashMap并且参数是一个hashMap的时候也会执行）
			inflateTable(threshold);
		}
		if (key == null) {
			//如果key为null 则执行 存放key 为null特殊处理的方法(存入下标为0的位置，若0的下标有值，则放入下标链表的头部)
			return putForNullKey(value);
		}
		//计算key的hash值
		int hash = hash(key);
		//计算key的下标
		int i = indexFor(hash, table.length);
		//遍历下标为i的链表
		for (Entry<K, V> e = table[i]; e != null; e = e.next) {
			Object k;
			//若 key已存在 则将原来的value的值替换为新的value并把旧的value返回
			if (e.hash == hash && ((k = e.key) == key || key.equals(k))) {
				V oldValue = e.value;
				e.value = value;
				e.recordAccess(this);
				return oldValue;
			}
		}
		modCount++;
		//若key不存在则 新建一个entry
		addEntry(hash, key, value, i);
		return null;
	}

	/**
	 * Offloaded version of put for null keys
	 *
	 * 将null值存储到hashMap。
	 */
	private V putForNullKey(V value) {
		//取table[0] 的值，判断该位置是否有存储值，
		for (Entry<K, V> e = table[0]; e != null; e = e.next) {
			//判断该地方的是否已经存储了一个key为null的值
			if (e.key == null) {
				//若已经存储一个key为null的值则将table[0]地方的value替换为新的value ，并将老的value返回
				V oldValue = e.value;
				e.value = value;
				//这个就基本没什么用 只是一个类似于钩子，需要其他类重写。
				e.recordAccess(this);
				return oldValue;
			}
		}
		modCount++;
		addEntry(0, null, value, 0);
		return null;
	}

	/**
	 * This method is used instead of put by constructors and
	 * pseudoconstructors (clone, readObject).  It does not resize the table,
	 * check for comodification, etc.  It calls createEntry rather than
	 * addEntry.
	 */
	private void putForCreate(K key, V value) {
		//获取key的hash值
		int hash = null == key ? 0 : hash(key);
		//获取key对应的下标值
		int i = indexFor(hash, table.length);

		/**
		 * Look for preexisting entry for key.  This will never happen for
		 * clone or deserialize.  It will only happen for construction if the
		 * input Map is a sorted map whose ordering is inconsistent w/ equals.
		 *
		 * 遍历新的数组的对应下标的链表若该位置已有值，则将value值替换
		 */
		for (Entry<K, V> e = table[i]; e != null; e = e.next) {
			Object k;
			if (e.hash == hash && ((k = e.key) == key || (key != null && key.equals(k)))) {
				e.value = value;
				return;
			}
		}
		//创建数组对象entry 并存入数组当中
		createEntry(hash, key, value, i);
	}

	/**
	 * 遍历原来的map  将其中的值存入新创建的map内
	 * @param m
	 */
	private void putAllForCreate(Map<? extends K, ? extends V> m) {
		for (Map.Entry<? extends K, ? extends V> e : m.entrySet())
			putForCreate(e.getKey(), e.getValue());
	}

	/**
	 * Rehashes the contents of this map into a new array with a
	 * larger capacity.  This method is called automatically when the
	 * number of keys in this map reaches its threshold.
	 *
	 * If current capacity is MAXIMUM_CAPACITY, this method does not
	 * resize the map, but sets threshold to Integer.MAX_VALUE.
	 * This has the effect of preventing future calls.
	 *
	 * @param newCapacity the new capacity, MUST be a power of two;
	 *        must be greater than current capacity unless current
	 *        capacity is MAXIMUM_CAPACITY (in which case value
	 *        is irrelevant).
	 *扩容方法
	 */
	void resize(int newCapacity) {
		//获取扩容之前的table
		Entry[] oldTable = table;
		//扩容之前的Capacity 这个时候如果确实需要扩容的话，size=Capacity
		int oldCapacity = oldTable.length;
		if (oldCapacity == MAXIMUM_CAPACITY) {
			//若扩容前端数组大小已经达到了最大值 则直接返回 不进行扩容
			/**
			 * 疑问：hashMap最大能存储多少数据？是Integer.MAX_VALUE个吗？
			 * 解答：1、自己测试存入Integer.MAX_VALUE+1个数据 直接就不执行(for 循环插入)了。
			 * 查询资料得：
			 * 1、元素个数可以超过int的最大值的，只不过通过get(int index)方法取不到index超过int最大值的元素了
			 * 2、如过考虑Hash冲突，由于HashMap使用“数组+链表”的方式存储数据，一个HashMap最多可以有(2^31-1)个链表.
			 *      一个链表可以包含多少个元素？由于64bit JVM的引用类型长度（可以）为8字节，所以一个链表最多可以存放(2^63-1)个元素。
			 *      HashMap，可以存放的最多元素是：(2^31-1)*(2^63-1)个？ 当然不是。一个JVM虚拟机里最多只能存在（2^63-1）个对象！！！原因前面已说，因为应用类型的长度为8字节。
			 *      由于JVM启动起来已经生成了很多对象，所以实际上我们写代码也不可能new（2^63-1）这么多个对象
			 *      上面没有考虑JVM实际上能使用多少内存，一个对象至少占用16字节，加上上引用8字节就是24字节了。所以实际上JVM能分配出来的对象远没有那么多。
			 */
			threshold = Integer.MAX_VALUE;
			return;
		}

		//创建一个大小为newCapacity的空数组
		Entry[] newTable = new Entry[newCapacity];
		//将所有的entry对象转移到新的table内
		transfer(newTable, initHashSeedAsNeeded(newCapacity));
		//table的值更换为newTable
		table = newTable;
		//计算新的扩容后的阈值
		threshold = (int) Math.min(newCapacity * loadFactor, MAXIMUM_CAPACITY + 1);
	}

	/**
	 * Transfers all entries from current table to newTable.
	 * 将所有的entry对象转移到新的table内
	 */
	void transfer(Entry[] newTable, boolean rehash) {
		//获取新的table数组的大小
		int newCapacity = newTable.length;
		//遍历旧的数组
		for (Entry<K, V> e : table) {
			/**
			 * 遍历数组内每一个链表
			 *  1、每次将链表清空，数组不清空
			 *  2、遍历链表的时候，从链表头部开始遍历，每次取头部节点追加到新的table内（数组内的链表和之前的顺序是相反的，因为是先取的头部节点，而插入的时候又是对链表头部插入）
			 */
			while (null != e) {
				//获取链表下一个节点元素
				Entry<K, V> next = e.next;
				//若rehash为true则进行重新计算hash值
				if (rehash) {
					e.hash = null == e.key ? 0 : hash(e.key);
				}
				//获取新的下标值
				int i = indexFor(e.hash, newCapacity);
				//将新数组下标为i的链表追加到该链表节点（执行完就是新的链表，符合hashMap链表是从头部插入的流程）
				e.next = newTable[i];
				//将新的链表赋值到下标为i的数组内（也就是说每次更新都是整个链表，而不是新table内元素链表的某个节点），该i也许在newTable内不是一个下标(因为重新计算了下标)
				newTable[i] = e;
				//将去除了本次遍历节点的链表赋值给e 继续遍历后面的链表节点
				e = next;
			}
		}
	}

	/**
	 * Copies all of the mappings from the specified map to this map.
	 * These mappings will replace any mappings that this map had for
	 * any of the keys currently in the specified map.
	 *
	 * @param m mappings to be stored in this map
	 * @throws NullPointerException if the specified map is null
	 *
	 * 直接put一个map
	 * 重点！！！
	 * 这里只是对于基础类型的深拷贝，对于引用类型的 比如说String  就不是深拷贝（这一点网上很多说的都是错误的）
	 * 若我们要实现hashMap的深拷贝 是使用序列化的方式来实现对象的深拷贝（我们的hashMap是实现了Serializable）
	 */
	public void putAll(Map<? extends K, ? extends V> m) {
		//获取添加的map的大小
		int numKeysToBeAdded = m.size();
		if (numKeysToBeAdded == 0)
			//若为0则直接返回
			return;
		if (table == EMPTY_TABLE) {
			//如果当前的table是个空数组，则执行初始化hash空间
			inflateTable((int) Math.max(numKeysToBeAdded * loadFactor, threshold));
		}

		/*
		 * Expand the map if the map if the number of mappings to be added
		 * is greater than or equal to threshold.  This is conservative; the
		 * obvious condition is (m.size() + size) >= threshold, but this
		 * condition could result in a map with twice the appropriate capacity,
		 * if the keys to be added overlap with the keys already in this map.
		 * By using the conservative calculation, we subject ourself
		 * to at most one extra resize.
		 */
		//如果新添加的size大于当前的一个扩容阈值则进行扩容
		if (numKeysToBeAdded > threshold) {
			//获取容量值 = size*负载因子
			int targetCapacity = (int) (numKeysToBeAdded / loadFactor + 1);
			if (targetCapacity > MAXIMUM_CAPACITY)
				//若超过最大容量，则以2^30
				targetCapacity = MAXIMUM_CAPACITY;
			int newCapacity = table.length;
			while (newCapacity < targetCapacity)
				newCapacity <<= 1;
			if (newCapacity > table.length)
				resize(newCapacity);
		}

		for (Map.Entry<? extends K, ? extends V> e : m.entrySet())
			put(e.getKey(), e.getValue());
	}

	/**
	 * Removes the mapping for the specified key from this map if present.
	 *
	 * @param  key key whose mapping is to be removed from the map
	 * @return the previous value associated with <tt>key</tt>, or
	 *         <tt>null</tt> if there was no mapping for <tt>key</tt>.
	 *         (A <tt>null</tt> return can also indicate that the map
	 *         previously associated <tt>null</tt> with <tt>key</tt>.)
	 * 移除元素方法
	 */
	public V remove(Object key) {
		Entry<K, V> e = removeEntryForKey(key);
		return (e == null ? null : e.value);
	}

	/**
	 * Removes and returns the entry associated with the specified key
	 * in the HashMap.  Returns null if the HashMap contains no mapping
	 * for this key.
	 */
	final Entry<K, V> removeEntryForKey(Object key) {
		if (size == 0) {
			return null;
		}
		int hash = (key == null) ? 0 : hash(key);
		int i = indexFor(hash, table.length);
		// pre 和e 都是链表头的值
		Entry<K, V> prev = table[i];
		Entry<K, V> e = prev;
		while (e != null) {
			//链表头节点的下一个节点
			Entry<K, V> next = e.next;
			Object k;
			// 若头节点=要移除的key
			if (e.hash == hash && ((k = e.key) == key || (key != null && key.equals(k)))) {
				modCount++;
				size--;
				if (prev == e)
					//移除头节点，将头下一个节点变为头节点
					table[i] = next;
				else
					//移除我们对于的那个节点 也就是此时的e，将我们上一个节点的next指向为下下一个节点
					prev.next = next;
				e.recordRemoval(this);
				return e;
			}
			prev = e;
			e = next;
		}

		return e;
	}

	/**
	 * Special version of remove for EntrySet using {@code Map.Entry.equals()}
	 * for matching.
	 */
	final Entry<K, V> removeMapping(Object o) {
		if (size == 0 || !(o instanceof Map.Entry))
			return null;

		Map.Entry<K, V> entry = (Map.Entry<K, V>) o;
		Object key = entry.getKey();
		int hash = (key == null) ? 0 : hash(key);
		int i = indexFor(hash, table.length);
		Entry<K, V> prev = table[i];
		Entry<K, V> e = prev;

		while (e != null) {
			Entry<K, V> next = e.next;
			if (e.hash == hash && e.equals(entry)) {
				modCount++;
				size--;
				if (prev == e)
					table[i] = next;
				else
					prev.next = next;
				e.recordRemoval(this);
				return e;
			}
			prev = e;
			e = next;
		}

		return e;
	}

	/**
	 * Removes all of the mappings from this map.
	 * The map will be empty after this call returns.
	 * 清空map里面所有的KV映射
	 */
	public void clear() {
		modCount++;
		//Arrays 是Array 的工具类；内部是循环遍历数组，将我们传递的null值赋值给数组的每一个下标对应的元素
		Arrays.fill(table, null);
		size = 0;
	}

	/**
	 * Returns <tt>true</tt> if this map maps one or more keys to the
	 * specified value.
	 *
	 * @param value value whose presence in this map is to be tested
	 * @return <tt>true</tt> if this map maps one or more keys to the
	 *         specified value
	 * 判断map内是否含有指定的value值
	 */
	public boolean containsValue(Object value) {
		if (value == null)
			//若value为null 则判断map内是否含有null值
			return containsNullValue();
		Entry[] tab = table;
		//遍历整个数组和链表
		for (int i = 0; i < tab.length; i++)
			for (Entry e = tab[i]; e != null; e = e.next)
				if (value.equals(e.value))
					return true;
		return false;
	}

	/**
	 * Special-case code for containsValue with null argument
	 * 判断hashmap是否有null值
	 */
	private boolean containsNullValue() {
		Entry[] tab = table;
		//遍历整个数组和链表
		for (int i = 0; i < tab.length; i++)
			for (Entry e = tab[i]; e != null; e = e.next)
				if (e.value == null)
					return true;
		return false;
	}

	/**
	 * Returns a shallow copy of this <tt>HashMap</tt> instance: the keys and
	 * values themselves are not cloned.
	 *
	 * @return a shallow copy of this map
	 * 返回此hashmap实例的浅层副本：
	 * 键和值本身不会被克隆.
	 *
	 * 大意就是：hashMap的clone方法时一个浅拷贝。
	 *  浅拷贝：对一个对象进行clone生成新的对象，新的对象要开辟一块新的内存来存储，新对象中的基本类型属性
	 *  和String类型属性都会开辟新的空间存储，但是如果是引用类型的属性，那这个引用类型的属性还是指向原对象
	 *  的引用属性内存，当对新的对象或原对象的引用属性做出改变的时候，两方的引用属性类型的值同时做出改变。
	 *
	 * 但是hashMap内的clone方法时有点特殊的：
	 *  他的table是新开辟的一个新的空间，也就是说他的table是非浅拷贝
	 *  但是他的数组对应的链表都是指向同一个内存地址，是浅拷贝。
	 */
	public Object clone() {
		HashMap<K, V> result = null;
		try {
			result = (HashMap<K, V>) super.clone();
		} catch (CloneNotSupportedException e) {
			// assert false;
		}
		if (result.table != EMPTY_TABLE) {
			result.inflateTable(Math.min(
					(int) Math.min(
							size * Math.min(1 / loadFactor, 4.0f),
							// we have limits...
							HashMap.MAXIMUM_CAPACITY),
					table.length));
		}
		result.entrySet = null;
		result.modCount = 0;
		result.size = 0;
		result.init();
		result.putAllForCreate(this);

		return result;
	}

	/**
	 *  map数组内的值，该值是一个单项链表结构(只有next，没有pre)
	 * @param <K>
	 * @param <V>
	 */
	static class Entry<K, V> implements Map.Entry<K, V> {
		// key值
		final K key;
		//value 值
		V value;
		//指向链表的下一个节点对象
		Entry<K, V> next;
		//key的hash值
		int hash;

		/**
		 * Creates new entry.
		 */
		Entry(int h, K k, V v, Entry<K, V> n) {
			value = v;
			next = n;
			key = k;
			hash = h;
		}

		public final K getKey() {
			return key;
		}

		public final V getValue() {
			return value;
		}

		public final V setValue(V newValue) {
			V oldValue = value;
			value = newValue;
			return oldValue;
		}

		/**
		 * 重写了 equals方法  只有当key  和value同时相等的时候，就认为这个entry是一样的
		 * @param o
		 * @return
		 */
		public final boolean equals(Object o) {
			if (!(o instanceof Map.Entry))
				return false;
			Map.Entry e = (Map.Entry) o;
			Object k1 = getKey();
			Object k2 = e.getKey();
			if (k1 == k2 || (k1 != null && k1.equals(k2))) {
				Object v1 = getValue();
				Object v2 = e.getValue();
				if (v1 == v2 || (v1 != null && v1.equals(v2)))
					return true;
			}
			return false;
		}

		public final int hashCode() {
			////key、value的位异或
			return Objects.hashCode(getKey()) ^ Objects.hashCode(getValue());
		}

		public final String toString() {
			return getKey() + "=" + getValue();
		}

		/**
		 * This method is invoked whenever the value in an entry is
		 * overwritten by an invocation of put(k,v) for a key k that's already
		 * in the HashMap.
		 * //当put覆盖原始数据时执行（供子类使用）
		 */
		void recordAccess(HashMap<K, V> m) {
		}

		/**
		 * This method is invoked whenever the entry is
		 * removed from the table.
		 * //当entry被删除时执行（供子类使用）
		 */
		void recordRemoval(HashMap<K, V> m) {
		}
	}

	/**
	 * Adds a new entry with the specified key, value and hash code to
	 * the specified bucket.  It is the responsibility of this
	 * method to resize the table if appropriate.
	 *
	 * Subclass overrides this to alter the behavior of put method.
	 */
	void addEntry(int hash, K key, V value, int bucketIndex) {
		//如果hashMap的大小和下一个扩容阈值 并且当前传值过来的下标不为null的时候 进行扩容
		if ((size >= threshold) && (null != table[bucketIndex])) {
			//扩容
			resize(2 * table.length);
			//重新计算扩容后的 hash值和下标
			/**
			 * 疑问：为什么要进行重新计算？
			 * 是因为之前计算的hash和下标都是相对于之前的容量来计算的，若还用之前的，则我们之后put进来的值还是很容易发生hash碰撞追加到链表上，
			 * 这样就导致我们的扩容没有意义了
			 */
			hash = (null != key) ? hash(key) : 0;
			bucketIndex = indexFor(hash, table.length);
		}
		//创建entry对象，并且将之前下标为bucketIndex的entry 放到next上。也就是说hashMap值进链表是从头部进入的
		createEntry(hash, key, value, bucketIndex);

	}

	/**
	 * Like addEntry except that this version is used when creating entries
	 * as part of Map construction or "pseudo-construction" (cloning,
	 * deserialization).  This version needn't worry about resizing the table.·
	 *
	 * Subclass overrides this to alter the behavior of HashMap(Map),
	 * clone, and readObject.
	 * 与 addEntry 方法类似，但是该方法只有在创建一个新的entry 作为map的一个子项的是，
	 * 该方法不必担心调整表的大小。
	 *
	 */
	void createEntry(int hash, K key, V value, int bucketIndex) {
		//获取数组下标为i的值
		Entry<K, V> e = table[bucketIndex];
		//以k v  hash 以及 下一个entry对象构建一个entry的链表，并且将该链表存入数组的 下标为i的位置
		/**
		 * 如果value是引用类型，这里就是引用传递
		 * 也就是验证了，在new HashMap(map) 、clone、readObject都是属于浅拷贝(value里面是引用类型的情况下，那为什么非引用类型就是深拷贝呢)
		 */
		table[bucketIndex] = new Entry<>(hash, key, value, e);
		//map容器大小+1
		size++;
	}

	private abstract class HashIterator<E> implements

	<E> {
		Entry<K, V> next;        // next entry to return
		int expectedModCount;   // For fast-fail
		int index;              // current slot
		Entry<K, V> current;     // current entry

		HashIterator() {
			expectedModCount = modCount;
			if (size > 0) { // advance to first entry
				Entry[] t = table;
				while (index < t.length && (next = t[index++]) == null)
					;
			}
		}

		public final boolean hasNext () {
			return next != null;
		}

		final Entry<K, V> nextEntry () {
			if (modCount != expectedModCount)
				throw new ConcurrentModificationException();
			Entry<K, V> e = next;
			if (e == null)
				throw new NoSuchElementException();

			if ((next = e.next) == null) {
				Entry[] t = table;
				while (index < t.length && (next = t[index++]) == null)
					;
			}
			current = e;
			return e;
		}

		public void remove () {
			if (current == null)
				throw new IllegalStateException();
			if (modCount != expectedModCount)
				throw new ConcurrentModificationException();
			Object k = current.key;
			current = null;
			HashMap.this.removeEntryForKey(k);
			expectedModCount = modCount;
		}
	}

	private final class ValueIterator extends HashIterator<V> {
		public V next() {
			return nextEntry().value;
		}
	}

	private final class KeyIterator extends HashIterator<K> {
		public K next() {
			return nextEntry().getKey();
		}
	}

	private final class EntryIterator extends HashIterator<Map.Entry<K, V>> {
		public Map.Entry<K, V> next() {
			return nextEntry();
		}
	}

	// Subclass overrides these to alter behavior of views' iterator() method
	Iterator<K> newKeyIterator() {
		return new KeyIterator();
	}

	Iterator<V> newValueIterator() {
		return new ValueIterator();
	}

	Iterator<Map.Entry<K, V>> newEntryIterator() {
		return new EntryIterator();
	}


	// Views

	private transient Set<Map.Entry<K, V>> entrySet = null;

	/**
	 * Returns a {@link Set} view of the keys contained in this map.
	 * The set is backed by the map, so changes to the map are
	 * reflected in the set, and vice-versa.  If the map is modified
	 * while an iteration over the set is in progress (except through
	 * the iterator's own <tt>remove</tt> operation), the results of
	 * the iteration are undefined.  The set supports element removal,
	 * which removes the corresponding mapping from the map, via the
	 * <tt>Iterator.remove</tt>, <tt>Set.remove</tt>,
	 * <tt>removeAll</tt>, <tt>retainAll</tt>, and <tt>clear</tt>
	 * operations.  It does not support the <tt>add</tt> or <tt>addAll</tt>
	 * operations.
	 */
	public Set<K> keySet() {
		Set<K> ks = keySet;
		return (ks != null ? ks : (keySet = new KeySet()));
	}

	private final class KeySet extends AbstractSet<K> {
		public Iterator<K> iterator() {
			return newKeyIterator();
		}

		public int size() {
			return size;
		}

		public boolean contains(Object o) {
			return containsKey(o);
		}

		public boolean remove(Object o) {
			return HashMap.this.removeEntryForKey(o) != null;
		}

		public void clear() {
			HashMap.this.clear();
		}
	}

	/**
	 * Returns a {@link Collection} view of the values contained in this map.
	 * The collection is backed by the map, so changes to the map are
	 * reflected in the collection, and vice-versa.  If the map is
	 * modified while an iteration over the collection is in progress
	 * (except through the iterator's own <tt>remove</tt> operation),
	 * the results of the iteration are undefined.  The collection
	 * supports element removal, which removes the corresponding
	 * mapping from the map, via the <tt>Iterator.remove</tt>,
	 * <tt>Collection.remove</tt>, <tt>removeAll</tt>,
	 * <tt>retainAll</tt> and <tt>clear</tt> operations.  It does not
	 * support the <tt>add</tt> or <tt>addAll</tt> operations.
	 */
	public Collection<V> values() {
		Collection<V> vs = values;
		return (vs != null ? vs : (values = new Values()));
	}

	private final class Values extends AbstractCollection<V> {
		public Iterator<V> iterator() {
			return newValueIterator();
		}

		public int size() {
			return size;
		}

		public boolean contains(Object o) {
			return containsValue(o);
		}

		public void clear() {
			HashMap.this.clear();
		}
	}

	/**
	 * Returns a {@link Set} view of the mappings contained in this map.
	 * The set is backed by the map, so changes to the map are
	 * reflected in the set, and vice-versa.  If the map is modified
	 * while an iteration over the set is in progress (except through
	 * the iterator's own <tt>remove</tt> operation, or through the
	 * <tt>setValue</tt> operation on a map entry returned by the
	 * iterator) the results of the iteration are undefined.  The set
	 * supports element removal, which removes the corresponding
	 * mapping from the map, via the <tt>Iterator.remove</tt>,
	 * <tt>Set.remove</tt>, <tt>removeAll</tt>, <tt>retainAll</tt> and
	 * <tt>clear</tt> operations.  It does not support the
	 * <tt>add</tt> or <tt>addAll</tt> operations.
	 *
	 * @return a set view of the mappings contained in this map
	 */
	public Set<Map.Entry<K, V>> entrySet() {
		return entrySet0();
	}

	private Set<Map.Entry<K, V>> entrySet0() {
		Set<Map.Entry<K, V>> es = entrySet;
		return es != null ? es : (entrySet = new EntrySet());
	}

	private final class EntrySet extends AbstractSet<Map.Entry<K, V>> {
		public Iterator<Map.Entry<K, V>> iterator() {
			return newEntryIterator();
		}

		public boolean contains(Object o) {
			if (!(o instanceof Map.Entry))
				return false;
			Map.Entry<K, V> e = (Map.Entry<K, V>) o;
			Entry<K, V> candidate = getEntry(e.getKey());
			return candidate != null && candidate.equals(e);
		}

		public boolean remove(Object o) {
			return removeMapping(o) != null;
		}

		public int size() {
			return size;
		}

		public void clear() {
			HashMap.this.clear();
		}
	}

	/**
	 * Save the state of the <tt>HashMap</tt> instance to a stream (i.e.,
	 * serialize it).
	 *
	 * @serialData The <i>capacity</i> of the HashMap (the length of the
	 *             bucket array) is emitted (int), followed by the
	 *             <i>size</i> (an int, the number of key-value
	 *             mappings), followed by the key (Object) and value (Object)
	 *             for each key-value mapping.  The key-value mappings are
	 *             emitted in no particular order.
	 */
	private void writeObject(java.io.ObjectOutputStream s)
			throws IOException {
		// Write out the threshold, loadfactor, and any hidden stuff
		s.defaultWriteObject();

		// Write out number of buckets
		if (table == EMPTY_TABLE) {
			s.writeInt(roundUpToPowerOf2(threshold));
		} else {
			s.writeInt(table.length);
		}

		// Write out size (number of Mappings)
		s.writeInt(size);

		// Write out keys and values (alternating)
		if (size > 0) {
			for (Map.Entry<K, V> e : entrySet0()) {
				s.writeObject(e.getKey());
				s.writeObject(e.getValue());
			}
		}
	}

	private static final long serialVersionUID = 362498820763181265L;

	/**
	 * Reconstitute the {@code HashMap} instance from a stream (i.e.,
	 * deserialize it).
	 */
	private void readObject(java.io.ObjectInputStream s)
			throws IOException, ClassNotFoundException {
		// Read in the threshold (ignored), loadfactor, and any hidden stuff
		s.defaultReadObject();
		if (loadFactor <= 0 || Float.isNaN(loadFactor)) {
			throw new InvalidObjectException("Illegal load factor: " +
					loadFactor);
		}

		// set other fields that need values
		table = (Entry<K, V>[]) EMPTY_TABLE;

		// Read in number of buckets
		s.readInt(); // ignored.

		// Read number of mappings
		int mappings = s.readInt();
		if (mappings < 0)
			throw new InvalidObjectException("Illegal mappings count: " +
					mappings);

		// capacity chosen by number of mappings and desired load (if >= 0.25)
		int capacity = (int) Math.min(
				mappings * Math.min(1 / loadFactor, 4.0f),
				// we have limits...
				HashMap.MAXIMUM_CAPACITY);

		// allocate the bucket array;
		if (mappings > 0) {
			inflateTable(capacity);
		} else {
			threshold = capacity;
		}

		init();  // Give subclass a chance to do its thing.

		// Read the keys and values, and put the mappings in the HashMap
		for (int i = 0; i < mappings; i++) {
			K key = (K) s.readObject();
			V value = (V) s.readObject();
			putForCreate(key, value);
		}
	}

	// These methods are used when serializing HashSets
	int capacity() {
		return table.length;
	}

	float loadFactor() {
		return loadFactor;
	}
}
